#!/bin/bash

set -x

get_used_folders() { #we assume the storage volume is in data
    mongo
}

echo "BASH: "  $(dig +short tasks.$SERVICENAME)
mapfile -t ip_list < <(dig +short tasks.$SERVICENAME)
len=${#ip_list[@]};
host_ips=$(hostname -I);


echo "BASH: " $host_ips;
echo "BASH: " ${ip_list[@]};

if [ $len -le 1 ]
then
    rm -rf "/mongo_data/0" || true
    mkdir -p /mongo_data/0
    #Initialise repSet and take the first folder
    mongod --port 27017 --dbpath=/mongo_data/0 --replSet "rs0"
else
    
    #Choose an available folder and Join the repSet
    for ip in "${ip_list[@]}"
    do
        if ! [[ $host_ips == *$ip* ]] # check if ip is own
        then 
            echo "BASH: " $(mongo "$ip" --eval "db.adminCommand({getLog: 'global'}).log" | grep -o 'dbPath: "[^"]*"' | awk '{print $2}' | tr -d '"')
            pathList="$pathList "$(mongo "$ip" --eval "db.adminCommand({getLog: 'global'}).log" | grep -o 'dbPath: "[^"]*"' | awk '{print $2}' | tr -d '"')
            if [ $(mongo "$ip" --eval "db.isMaster().ismaster" | grep -o 'true' | wc -l) -ge 1 ]
            then
              ip_master=$ip
            fi
        fi
    done
    echo "BASH: $pathList"
    for mpath in /mongo_data/*
    do
      if ! [[ "$pathList" == *"$mpath"* ]]
      then
         rm -rf "${mpath}"
         DBPATH=$mpath
      fi
    done
    if [ -z "$DBPATH" ]
    then
       DBPATH=/mongo_data/$(( $(ls /mongo_data | wc -l)))
    fi
    mkdir $DBPATH
    mask=$(echo $ip_master | grep -o "[0-9]*\.[0-9]*\.[0-9]*\.")
    good_ip=$(echo $host_ips | grep -o "$mask[0-9]*")
    mongo "$ip_master" --eval 'rs.add( "'$good_ip':27017" )'
    mongod --port 27017 --dbpath=$DBPATH --replSet "rs0"
fi
