FROM arm7/mongo
RUN apt update
RUN apt install -y dnsutils
COPY init-mongo.sh /
ENTRYPOINT [ "/bin/bash", "/init-mongo.sh" ]
